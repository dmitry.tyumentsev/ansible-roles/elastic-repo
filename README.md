Elastic repository role
=========

Setup elastic repository on Debian/Ubuntu servers.

Requirements
-----------------
The repository is not available for users from Russia

Use yandex mirror repository
```yml
elastic_apt_repository: deb [signed-by={{ _elastic_keyring_path }}] https://mirror.yandex.ru/mirrors/elastic/8/ stable main
elastic_apt_gpg_key_url: https://keyserver.ubuntu.com/pks/lookup?op=get&search=0x46095acc8548582c1a2699a9d27d666cd88e42b4
```


Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - tyumentsev4.elastc_repo
```
